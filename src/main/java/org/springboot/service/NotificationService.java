package org.springboot.service;

import org.springboot.domain.Notification;
import org.springframework.data.domain.Page;

public interface NotificationService {

	public Notification saveNotification(Notification notification);

	public Page<Notification> getLastNotificationsByAdmin(int idAdmin, int page, int size);
}
