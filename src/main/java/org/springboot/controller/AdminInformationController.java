package org.springboot.controller;

import org.springboot.domain.AdminInformation;
import org.springboot.service.AdminInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminInformationController {
	
	@Autowired
	AdminInformationService adminInformationService;
	
	@PostMapping("/saveAdmin")
	public AdminInformation saveAdmin(@RequestBody AdminInformation adminInformation) {
		return adminInformationService.addAdmin(adminInformation);
	}

}
