package org.springboot.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springboot.domain.Notification;
import org.springboot.repository.NotificationRepository;
import org.springboot.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class NotificationController {

	@Autowired
	private NotificationService notificationService;

	@Autowired
	NotificationRepository notificationRepository;

	@PostMapping("/saveNotif")
	public Notification saveNotification(@RequestBody Notification notification,
			@RequestParam(name = "timeOfNotif") String timeOfNotif) {
		System.out.println("entre to save function");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		LocalDateTime localDateTimeOfNotif = LocalDateTime.parse(timeOfNotif, formatter);
		System.out.println("date is     " + localDateTimeOfNotif);

		if (notification.getNotificationTime() == null)
			notification.setActualTime();
		notification.setNotificationTime(localDateTimeOfNotif);
		return notificationService.saveNotification(notification);
	}

	@GetMapping("/getNotifs")
	public List<Notification> notifs() {
		return notificationRepository.findAll();
	}
	
	@GetMapping("/getLastNotifByAdmin")
	public Page<Notification> getLastNotifByAdmin(@RequestParam(name="id") int id){
		return notificationService.getLastNotificationsByAdmin(id, 0, 10);
	}
	

	
//	@GetMapping(value = "/getLastNotifByAdmin")
//	  HttpEntity<PagedResourcesAssembler<Notification>> pagedNotifications(Pageable pageable,
//	    PagedResourcesAssembler<Notification> assembler) {
//
//	    Page<Notification> persons = notificationRepository.findAll(pageable);
//	    return new ResponseEntity<>(assembler.toResources(persons), HttpStatus.OK);
//	  }
	
}
