package org.springboot.app;




import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
//@PropertySource({"classpath:application.properties"})
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@ComponentScan(basePackages = {"org.springboot.domain", "org.springboot.controller" , "org.springboot.service", "org.springboot.serviceImpl"})
@EnableJpaRepositories("org.springboot.repository")
@EntityScan(basePackages = { "org.springboot.domain" })
@SpringBootApplication
public class DemoApplication {

	
	public static void main(String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

}



