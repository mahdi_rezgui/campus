package org.springboot.domain;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@EntityListeners(TimeListener.class)
@Entity
@Table(name="CAMPUS")
public class Campus {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_campus")
	private int idCampus;
	
	@Column(name="campus_name")
	private String name;
	
	@Column(name="campus_location")
	private String location;
	
	//@Convert(converter = LocalDatePersistenceConverter.class)
	@Column(name="campus_creation_date")
	private LocalDate dateOfCreation;
	
	@OneToMany(mappedBy="campus")
	private List<University> universityList;
	
	@OneToOne
	@JoinColumn(name = "admin_id")
	private Admin admin;
	
	public Campus() {
		super();
	}


	public Campus(int idCampus, String name, String location, LocalDate dateOfCreation, Admin admin) {
		super();
		this.idCampus = idCampus;
		this.name = name;
		this.location = location;
		this.dateOfCreation = dateOfCreation;
		this.admin = admin;
	}
	
	
	public Campus(String name, String location, LocalDate dateOfCreation) {
		super();
		this.name = name;
		this.location = location;
		this.dateOfCreation = dateOfCreation;
	}

	

	public int getIdCampus() {
		return idCampus;
	}
	public void setIdCampus(int idCampus) {
		this.idCampus = idCampus;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public LocalDate getDateOfCreation() {
		return dateOfCreation;
	}
	public void setDateOfCreation(LocalDate dateOfCreation) {
		if(dateOfCreation == null) {
			this.dateOfCreation = LocalDate.now();
		}
			
		this.dateOfCreation = dateOfCreation;
	}


	public List<University> getUniversityList() {
		return universityList;
	}


	public void setUniversityList(List<University> universityList) {
		this.universityList = universityList;
	}


	public Admin getAdmin() {
		return admin;
	}


	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	
	
	
}
