package org.springboot.service;

import org.springboot.domain.University;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UniversityService {

	public University getUniversity(int idUniversity);
	public Page<Integer> getUniversities(Pageable pageable);

}
