package org.springboot.controller;

import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springboot.domain.Candidate;
import org.springboot.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import au.com.bytecode.opencsv.CSVReader;

@RestController
public class CandidateController {

	private static final Logger logger = LoggerFactory.getLogger(AdminInformationController.class);

	@Autowired
	CandidateService candidateService;

	private String UPLOADED_FOLDER = "/home/mahdi/Téléchargements/";

	private CSVReader reader;

	@PostMapping("/registerCandidate")
	public ResponseEntity<Candidate> registerCandidate(@RequestBody @Valid Candidate canidateToRegister) {

		try {
			candidateService.saveCandidate(canidateToRegister);
			return new ResponseEntity<Candidate>(canidateToRegister, HttpStatus.OK);
		} catch (Exception e) {
			logger.error("************EXCEPTION REGISTER CANDIDATE*********", e);
			return new ResponseEntity<Candidate>(HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/importJsenCSV")
	public void importJsenCSV() throws Exception {
		reader = new CSVReader(new FileReader(UPLOADED_FOLDER + "j-sen_sample_data_and_code - Data Field.csv"), ';',
				'"', 0);

		// Read all rows at once
		List<String[]> allRows = reader.readAll();

		String[] row1 = allRows.get(1);
		System.out.println(row1[1]);
		// Read CSV line by line and use the string array as you want
		// for (String[] row : allRows) {
		// System.out.println(Arrays.toString(row));
		// row[1].toString()
		// }
	}

	@GetMapping("/getCandidateById/{id}")
	public Candidate getCandidateById(@PathVariable("id") int id) {
		return candidateService.getCandidateById(id);
	}

	@GetMapping("/getPattern")
	// public Boolean patternTest(@RequestParam(name = "number") String number) {

	public String patternTest() throws ParseException {

		String mydata = "10時00分sfv〜qs24時05分(シフト制)";
		Pattern pattern = Pattern.compile("([0-9]+)(時)([0-9]+)");
		Matcher matcher = pattern.matcher(mydata);
		int i = 0;
		String startTime = null;
		String finishTime = null;

		while (matcher.find()) {

			if (i == 0) {
				startTime = matcher.group(1) + ":" + matcher.group(3);
			} else if (i == 1) {
				finishTime = matcher.group(1) + ":" + matcher.group(3);
			}
			i++;

		}

		DateFormat formatter = new SimpleDateFormat("HH:mm");
		Date startWorkingTimeDate = formatter.parse(startTime);
		Date finishWorkingTimeDate = formatter.parse(finishTime);

		System.out.println(startTime + "\n");
		System.out.println(finishTime);

		return startWorkingTimeDate.toString() + "***" + finishWorkingTimeDate.toString();

		// [^\\d]*[\\d]+[^\\d]+([\\d]+) Pattern p = Pattern.compile("\\d+");
//		Pattern p = Pattern.compile("[^\\d]*[\\d]+[^\\d]+([\\d]+)");
//		Matcher m = p.matcher("10時00分〜24時00分(シフト制)");
//		while (m.find()) {
//			System.out.println(m.group());
//		}
//		String str = "abcd1234sd12";
//		String[] part = str.split("(?<=\\D)(?=\\d)");
//		
//		System.out.println(part[0]);
//		System.out.println(part[1]);
//		System.out.println(part[2]);
		// System.out.println(part[3]);

//		System.out.println(m.find());
//		if (m.find()) {
//			System.out.println(m.group(0));
//
//			System.out.println(m.group(1));
//			System.out.println(m.group(2));
//			System.out.println(m.group(3));
//			System.out.println(m.group(4));
		// }

//		String regexCondition = "(69|[7-9][0-9])";
//		Pattern patternCondition = Pattern.compile(regexCondition);
//		Matcher matcher = patternCondition.matcher(number);

		// System.out.println(matcher.find());

		// return matcher.find();
//		String workingTime = "10時30分〜24時00分(シフト制)";
//		String[] parts = workingTime.split("〜");
//		String part1 = parts[0];
//		String part2 = parts[1];
//		String regexCondition = "([0-9]+時[0-9]+分)";

		// start time

//		System.out.println(part1 + " +++ " + part2);
//		Pattern patternCondition = Pattern.compile(regexCondition);
//		Matcher matcher = patternCondition.matcher();

//		String beforeDelim = null;
//		String afterDelim = null;
//		
//		final String regexConditionOne = "([0-9]+時[0-9]+分)";
//		
//		final Pattern patternConditionOne = Pattern.compile(regexConditionOne,
//				Pattern.MULTILINE);

		// Pattern p = Pattern.compile("^([a-zA-Z]+)([0-9]+)(.*)");

		// String workTime = "13時00分〜翌1時00分(シフト制)";
//		String workTime = "3610時00分〜22時30分(シフト制)";
//
//		Pattern pattern_1 = Pattern.compile("([0-9]+)(時)([0-9]+)(分〜)([0-9]+)(時)([0-9]+)(分)");
//		Pattern pattern_2 = Pattern.compile("([0-9]+)(時)([0-9]+)(分〜翌)([0-9]+)(時)([0-9]+)(分)");
//
//		Matcher matcher_1 = pattern_1.matcher(workTime);
//		Matcher matcher_2 = pattern_2.matcher(workTime);
//
//		String startWorkingTime = null;
//		String finishWorkingTime = null;
//
//		if (matcher_1.find()) {
//			System.out.println("11111111111");
//			startWorkingTime = matcher_1.group(1) + ":" + matcher_1.group(3) + ":00";
//			finishWorkingTime = matcher_1.group(5) + ":" + matcher_1.group(7) + ":00";
//		} else if (matcher_2.find()) {
//			System.out.println("2222222222");
//			startWorkingTime = matcher_2.group(1) + ":" + matcher_2.group(3) + ":00";
//			finishWorkingTime = matcher_2.group(5) + ":" + matcher_2.group(7) + ":00";
//		} else {
//			startWorkingTime = "00:00:00";
//			finishWorkingTime = "23:59:00";
//		}
//		System.out.println(startWorkingTime + "\n" + finishWorkingTime);

		// Pattern pattern_1 =
		// Pattern.compile("([0-9]+)(時)([0-9]+)(分〜)([0-9]+)(時)([0-9]+)(分)");
		// Pattern pattern_2 =
		// Pattern.compile("([0-9]+)(時)([0-9]+)(分〜翌)([0-9]+)(時)([0-9]+)(分)");
		//
		// Matcher matcher_1 = pattern_1.matcher(workTime);
		// Matcher matcher_2 = pattern_2.matcher(workTime);
		//
		// String startWorkingTime = null;
		// String finishWorkingTime = null;
		//
		// if (matcher_1.find()) {
		// System.out.println("11111111");
		// startWorkingTime = matcher_1.group(1) + ":" + matcher_1.group(3) + ":00";
		// finishWorkingTime = matcher_1.group(5) + ":" + matcher_1.group(7) + ":00";
		// } else if (matcher_2.find()) {
		// System.out.println("222222222");
		// startWorkingTime = matcher_2.group(1) + ":" + matcher_2.group(3) + ":00";
		// finishWorkingTime = matcher_2.group(5) + ":" + matcher_2.group(7) + ":00";
		// } else {
		// System.out.println("33333333333");
		// startWorkingTime = "00:00:00";
		// finishWorkingTime = "23:59:00";
		// }

	}
}
