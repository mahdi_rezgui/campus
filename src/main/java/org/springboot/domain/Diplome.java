package org.springboot.domain;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DIPLOME")
public class Diplome implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private int id;
	 private LocalDate dateObtention;
	 private String description;
	 private String speciality;
	 private String level; // LMD, Master, engineer, doctor ...
	 private String faculty;
	 private double average;
	 
	 
	public Diplome(int id, LocalDate dateObtention, String description, String speciality, String level, String faculty,
			double average) {
		super();
		this.id = id;
		this.dateObtention = dateObtention;
		this.description = description;
		this.speciality = speciality;
		this.level = level;
		this.faculty = faculty;
		this.average = average;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDate getDateObtention() {
		return dateObtention;
	}
	public void setDateObtention(LocalDate dateObtention) {
		this.dateObtention = dateObtention;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSpeciality() {
		return speciality;
	}
	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getFaculty() {
		return faculty;
	}
	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}
	 
	 
	 
}
