package org.springboot.service;

import java.util.List;

import org.springboot.domain.AdminInformation;
import org.springboot.domain.UserInformation;

public interface AdminInformationService {

	public List<AdminInformation> getAdmins();

	public AdminInformation addAdmin(AdminInformation adminInformation);

	public boolean approveUserRegistration(UserInformation userInformation);

}
