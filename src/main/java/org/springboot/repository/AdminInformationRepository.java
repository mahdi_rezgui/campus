package org.springboot.repository;

import org.springboot.domain.AdminInformation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminInformationRepository extends JpaRepository<AdminInformation, Long> {

}
