package org.springboot.serviceImpl;

import java.util.List;

import org.springboot.domain.Candidate;
import org.springboot.repository.CandidateRepository;
import org.springboot.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidateServiceImpl implements CandidateService {

	@Autowired
	CandidateRepository candidateRepository;

	@Override
	public List<Candidate> getAllCandidates() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Candidate> getLastRegistredCandidates() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Candidate saveCandidate(Candidate candidate) {
		return candidateRepository.save(candidate);
	}

	@Override
	public Candidate getCandidateById(int id) {
		return candidateRepository.findOne((long) id);
	}

}
