package org.springboot.repository;

import java.io.Serializable;

import org.springboot.domain.Notification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Serializable> {

	@Query(value = "select n from Notification n where n.adminInformation.idUser=:idAdmin and n.notificationTime = LOCALTIME")
	public Page<Notification> getLastNotificationsByAdmin(@Param("idAdmin") int idAdmin, Pageable pageable);

}
