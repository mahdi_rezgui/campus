package org.springboot.service;

import java.util.List;

import org.springboot.domain.Campus;

public interface CampusService {

	public List<Campus> findAll();
	public Campus getCampus(int idCampus);
	public int createCampus(Campus campus);
	public List<Campus> getSpecificCampus(String location);
	public List<Campus> getSpecificCampusByName(StringBuffer name);
	
}
