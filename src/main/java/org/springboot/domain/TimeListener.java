package org.springboot.domain;

import javax.persistence.PostPersist;


import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class TimeListener {

	public  String var = "* 1 * * * *"; // templateDate
	//public static int idCampus;
	
	@PostPersist
	public void campusPostPersist(Campus ob) {
		
		System.out.println("Listening User Post Persist : " + ob.getIdCampus());
		
	//	idCampus = ob.getIdCampus();

		//var = "0/15 * * * * ?" + ob.getName(); // templateDate that have changed
		
		var = "0/15 * * * * ?";
				
		// getcron();  
		//heloo();
		
	}
	
//	@Bean
//	public  String getcron() {
//		
//		// get the actual templateDate from the tempateDate static variable
//		
//		System.out.println(var+"22222222222222222");
//		
//		return var;
//		//return "0/15 * * * * ?";  // return the convinient to=ime for the cron
//	}
	
	
	
	//@Scheduled(cron = "#{getcron}")
	public void heloo(){
		
		System.out.println("111111111111111111111111111111111 " + var);
		
	}
}
