package org.springboot.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="UNIVERSITY")
public class University implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
//	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator="university_seq")
//	@SequenceGenerator(
//			name="campus_seq",
//			sequenceName="campus_sequence",
//			allocationSize=3
//			)
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name="university_id")
	private int id;
	@Column(name="university_name")
	private String name;
	@Column(name="url")
	private String url;
	@Column(name="phone_number")
	private String phoneNumber;
	@Column(name="email")
	private String email;
	@Column(name="location")
	private String location;
	@Column(name="creation_date")
	private Date dateOfCreation;
	@Column(name="description")
	private String description;
	@Column(name="rank")
	private int rank;
	@Column(name="area")
	private double area;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
	@JoinColumn(name = "campus_id")
	@JsonIgnore
	private Campus campus;
	
	public University(int id, String name, String url, String phoneNumber, String email, String location,
			Date dateOfCreation, String description, int rank, double area, Campus campus) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.location = location;
		this.dateOfCreation = dateOfCreation;
		this.description = description;
		this.rank = rank;
		this.area = area;
		this.campus = campus;
	}
	
	
	public University() {
		super();
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getDateOfCreation() {
		return dateOfCreation;
	}
	public void setDateOfCreation(Date dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Campus getCampus() {
		return campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}
	
}
