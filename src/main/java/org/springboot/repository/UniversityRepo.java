package org.springboot.repository;

import org.springboot.domain.Campus;
import org.springboot.domain.University;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UniversityRepo extends JpaRepository<University, Long>{

	@Query(value = "select u from University u where u.id =:idUniversity")
	public University getUniversity(@Param("idUniversity") int idUniversity);
	//Page<University> findByCampusId(int campusId, Pageable pageable);
	
	@Query(value = "select u.name, u.description from University u")
	public Page<Integer> getUniversities(Pageable pageable);
}
