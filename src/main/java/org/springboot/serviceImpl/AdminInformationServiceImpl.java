package org.springboot.serviceImpl;

import java.util.List;

import org.springboot.domain.AdminInformation;
import org.springboot.domain.UserInformation;
import org.springboot.repository.AdminInformationRepository;
import org.springboot.service.AdminInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminInformationServiceImpl implements AdminInformationService {

	@Autowired
	AdminInformationRepository adminInformationRepository;

	@Override
	public List<AdminInformation> getAdmins() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AdminInformation addAdmin(AdminInformation adminInformation) {
		return adminInformationRepository.save(adminInformation);
	}

	@Override
	public boolean approveUserRegistration(UserInformation userInformation) {
		// TODO Auto-generated method stub
		return false;
	}

}
