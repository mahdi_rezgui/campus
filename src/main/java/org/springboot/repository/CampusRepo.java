package org.springboot.repository;

import java.util.List;

import org.springboot.domain.Campus;
import org.springboot.domain.University;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CampusRepo extends JpaRepository<Campus, Long>{

	/**
	 * 
	 * @param idCampus: the id of the campus
	 * @return Campus with a given id
	 */
	@Query(value = "select c from Campus c where c.idCampus =:idCampus")
	public Campus getCampus(@Param("idCampus") int idCampus);
	
	/**
	 * 
	 * @param location: the location of the campus
	 * @return List of campus by key word for location
	 */
	@Query(value = "SELECT c FROM Campus c WHERE c.location like %:location%") // like %?1%
	public List<Campus> getSpecificCampus(@Param("location") String location);
	
	/**
	 * 
	 * @param nameCamp: the name of the campus
	 * @return List of campus by their name
	 */
	//@Query(value = "SELECT * FROM Campus c WHERE c.name =:nameCamp ",nativeQuery=true)
	@Query(value = "SELECT c FROM Campus c WHERE c.name like %:nameCamp%")
	public List<Campus> getSpecificCampusByName(@Param("nameCamp") StringBuffer nameCamp);
	
	@Query(value = "SELECT c.universityList FROM Campus c WHERE c.idCampus =:idCampus")
	public List<University> getUniversitiesOfSpecificCampus(@Param("idCampus")int idCampus);
	
	
	@Query(value = "SELECT * FROM Campus, ?#{#pageable}", nativeQuery = true)
	public Page<Campus> getAllCampus(Pageable pageable);
	
}
