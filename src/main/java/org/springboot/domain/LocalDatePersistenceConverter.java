package org.springboot.domain;

import java.sql.Date;
import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalDatePersistenceConverter implements AttributeConverter<LocalDate, Date> {
	
	@Override
	public java.sql.Date convertToDatabaseColumn(LocalDate entityValue) {

		if (entityValue == null) {

			entityValue = LocalDate.now();
		}

		return java.sql.Date.valueOf(entityValue);

	}

	@Override
	public LocalDate convertToEntityAttribute(java.sql.Date databaseValue) {

		return databaseValue.toLocalDate();

	}
}
