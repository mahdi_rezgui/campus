package org.springboot.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "STUDENT")
@PrimaryKeyJoinColumn(name = "person_id")
public class Student extends Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String notes;
	private int average;
	


	public Student() {
		super();
	}

	public Student(int id, String firstName, String lastName, String email, String phone, LocalDate birthDate,
			String adress, String login, String password) {
		super(id, firstName, lastName, email, phone, birthDate, adress, login, password);
	}

	public int getAverage() {
		return average;
	}

	public void setAverage(int average) {
		this.average = average;
	}

}
