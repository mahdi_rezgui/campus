package org.springboot.domain;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ADMIN")
@PrimaryKeyJoinColumn(name = "person_id")
public class Admin extends Person {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToOne(mappedBy = "admin")
	private Campus campus;

	private Double salary;

	public Admin(int id, String first_name, String lastName, String email, String phone, LocalDate birthDate,
			String adress, String login, String password, Campus campus, Double salary) {
		super(id, first_name, lastName, email, phone, birthDate, adress, login, password);
		this.campus = campus;
		this.salary = salary;
	}

	public Admin(int id, String first_name, String lastName, String email, String phone, LocalDate birthDate,
			String adress, String login, String password) {
		super(id, first_name, lastName, email, phone, birthDate, adress, login, password);
	}

	public Admin() {
		super();
	}

	public Campus getCampus() {
		return campus;
	}

	public void setCampus(Campus campus) {
		this.campus = campus;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

}
