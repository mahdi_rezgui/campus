package org.springboot.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Notification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idNotification;
	
	private String notificationText;
	
	private LocalDateTime notificationTime;
	
	private boolean read = false;
	
	@ManyToOne
	@JoinColumn(name = "idAdmin", referencedColumnName = "idUser")
	@JsonIgnoreProperties({"notifications"})
	private AdminInformation adminInformation;

	public int getIdNotification() {
		return idNotification;
	}

	public void setIdNotification(int idNotification) {
		this.idNotification = idNotification;
	}

	public String getNotificationText() {
		return notificationText;
	}

	public void setNotificationText(String notificationText) {
		this.notificationText = notificationText;
	}

	public LocalDateTime getNotificationTime() {
		return notificationTime;
	}

	public void setNotificationTime(LocalDateTime notificationTime) {
		this.notificationTime = notificationTime;
	}
	
	public void setActualTime() {
		//if(notificationTime == null)
			this.notificationTime = LocalDateTime.now();
		System.out.println("notificationTime-------: "+notificationTime);
		
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public AdminInformation getAdmin() {
		return adminInformation;
	}

	public void setAdmin(AdminInformation adminInformation) {
		this.adminInformation = adminInformation;
	}
	
	
	
}
