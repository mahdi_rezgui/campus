package org.springboot.serviceImpl;

import org.springboot.domain.University;
import org.springboot.repository.UniversityRepo;
import org.springboot.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UniversityServiceImpl implements UniversityService{

	@Autowired
	UniversityRepo universityRepo;
	
	@Override
	public University getUniversity(int idUniversity) {
		
		return universityRepo.getUniversity(idUniversity);
	}

	@Override
	public Page<Integer> getUniversities(Pageable pageable) {
		
		return universityRepo.getUniversities(pageable);
	}

}
