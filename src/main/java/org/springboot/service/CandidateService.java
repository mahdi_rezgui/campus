package org.springboot.service;

import java.util.List;

import org.springboot.domain.Candidate;

public interface CandidateService {

	List<Candidate> getAllCandidates();

	List<Candidate> getLastRegistredCandidates();

	Candidate saveCandidate(Candidate candidate);

	Candidate getCandidateById(int id);
}
