package org.springboot.serviceImpl;

import java.util.List;

import org.springboot.domain.Campus;
import org.springboot.repository.CampusRepo;
import org.springboot.service.CampusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CampusServiceImpl implements CampusService {

    @Autowired
    private CampusRepo campusRepository;
    
	@Override
	public List<Campus> findAll() {
		List<Campus> campusList = (List<Campus>) campusRepository.findAll();

		return campusList;
	}

	@Override
	public Campus getCampus(int idCampus) {
		
		return campusRepository.getCampus(idCampus);
	}

	@Override
	public int createCampus(Campus campus) {
		
			Campus campusToCreate = campusRepository.save(campus);
			return campusToCreate.getIdCampus();
		
	}

	@Override
	public List<Campus> getSpecificCampus(String location) {
		
		return campusRepository.getSpecificCampus(location);
	}

	@Override
	public List<Campus> getSpecificCampusByName(StringBuffer name) {
		
		System.out.println("Test for commit");
		return campusRepository.getSpecificCampusByName(name);
		
	}
	
	

}
