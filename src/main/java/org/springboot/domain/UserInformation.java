package org.springboot.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class UserInformation {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int idUser;
	StringBuilder firstName;
	StringBuilder lastName;
	StringBuilder email;
	StringBuilder messageInfo;
	int age;

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public StringBuilder getFirstName() {
		return firstName;
	}

	public void setFirstName(StringBuilder firstName) {
		this.firstName = firstName;
	}

	public StringBuilder getLastName() {
		return lastName;
	}

	public void setLastName(StringBuilder lastName) {
		this.lastName = lastName;
	}

	public StringBuilder getEmail() {
		return email;
	}

	public void setEmail(StringBuilder email) {
		this.email = email;
	}

	public StringBuilder getMessageInfo() {
		return messageInfo;
	}

	public void setMessageInfo(StringBuilder messageInfo) {
		this.messageInfo = messageInfo;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
