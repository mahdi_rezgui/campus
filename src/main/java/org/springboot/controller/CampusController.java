package org.springboot.controller;

import java.util.List;

import org.springboot.domain.Campus;
import org.springboot.domain.University;
import org.springboot.repository.CampusRepo;
import org.springboot.service.CampusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CampusController {

	@Autowired
	CampusService campusService;

	@Autowired
	CampusRepo campusRepo;

	/**
	 * @author MAHDI
	 * @return the list of all campus
	 */
	@CrossOrigin(origins = "*")
	@SuppressWarnings("unchecked")
	@GetMapping("/getAllCampus")
	public List<Campus> getCampusList() {

		List<Campus> campusList = campusService.findAll();

		if (campusList.isEmpty()) {
			Campus campus = new Campus("Campus el Manar", "Tunis El Manar", null);
			return (List<Campus>) campus;
		}

		return campusList;
	}

	@GetMapping("/allCampus")
	public Page<Campus> getAllCampus(@RequestParam(name = "numPage") int numPage,@RequestParam(name = "size") int size) {
		
		List<Campus> listCamp = campusRepo.findAll();
		Pageable pageable = new PageRequest(numPage, size);
		int start = pageable.getOffset();
		int end = (start + pageable.getPageSize()) > listCamp.size() ? listCamp.size() : (start + pageable.getPageSize());
		Page<Campus> pages = new PageImpl<Campus>(listCamp.subList(start, end), pageable, listCamp.size());
		return pages;
		
	}

	@GetMapping("/all")
	public Page<Campus> getAll() {

		Pageable pageable = new PageRequest(0, 20);
		return campusRepo.getAllCampus(pageable);

	}

	/**
	 * @author MAHDI
	 * @param idCampus:
	 *            the identifier of the campus requested
	 * @return the campus element
	 */

	@GetMapping("/getCampus")
	public Campus getCampus(@RequestParam(name = "idCampus") int idCampus) {

		return campusService.getCampus(idCampus);

	}

	@GetMapping("/getUniversitiesOfSpecificCampus")
	public List<University> getUniversitiesOfSpecificCampus(@RequestParam(name = "idCampus") int idCampus) {

		return campusRepo.getUniversitiesOfSpecificCampus(idCampus);

	}

	/**
	 * @author MAHDI
	 * @param campus
	 * @return the id of the campus
	 */

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/createCampus", method = RequestMethod.POST)
	public int createCampus(@RequestBody Campus campus) {

		return campusService.createCampus(campus);

	}

	@PutMapping("/campus/{campusId}")
	public Campus updateCampus(@PathVariable int campusId, @RequestBody Campus campusRequest) {

		// Campus campus = campusRepo.findOne(new Long(campusId));

		Campus campus = campusService.getCampus(campusId);
		if (campusRequest.getName() != null) {
			campus.setName(campusRequest.getName());
		}
		if (campusRequest.getLocation() != null) {
			campus.setLocation(campusRequest.getLocation());
		}
		if (campusRequest.getDateOfCreation() != null) {
			campus.setDateOfCreation(campusRequest.getDateOfCreation());
		}

		return campusRepo.save(campus);
	}

	/**
	 * @author MAHDI
	 * @param location:
	 *            the location of the requested campus
	 * @return the list of the campus by their location
	 */
	@GetMapping("/getListSpecific")
	public List<Campus> getSpecificCampus(@RequestParam String location) {

		return campusService.getSpecificCampus(location);

	}

	/**
	 * @author MAHDI
	 * @param nameCamp:
	 *            the name of the desired campus
	 * @return the list of the campus by their name
	 */

	@GetMapping("/getListSpecificByName")
	public List<Campus> getSpecificCampusByName(@RequestParam StringBuffer nameCamp) {

		return campusService.getSpecificCampusByName(nameCamp);
	}

}
