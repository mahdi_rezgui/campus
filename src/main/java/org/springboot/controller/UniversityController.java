package org.springboot.controller;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.springboot.domain.Campus;
import org.springboot.domain.University;
import org.springboot.repository.UniversityRepo;
import org.springboot.service.CampusService;
import org.springboot.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UniversityController {

	@Autowired
	UniversityRepo universityRepo;

	@Autowired
	CampusService campusService;
	
	@Autowired
	UniversityService universityService;

	@PostMapping("/university")
	public University createUniversity(@Valid @RequestBody University university,
			@RequestParam(name = "idCampus") int idCampus) {
		university.setCampus(campusService.getCampus(idCampus));
		return universityRepo.save(university);
	}

	@GetMapping("/allUniversities")
	public Page<University> getAllUniversities(Pageable pageable) {

		return universityRepo.findAll(pageable);

	}

	@PostMapping("/linkUniversity")
	public University linkUniversity(
			@RequestParam(name = "idUniversity") int idUniversity) {

		University university = universityService.getUniversity(idUniversity);
		//Campus campuss = campusService.getCampus(idCampus);
		Campus campuss = new Campus("campus of Ain Drahem", "Jendouba", LocalDate.now());
		university.setCampus(campuss);
		universityRepo.save(university);
		return university;
	}

//	@PostMapping(path = "/linkUniversity/{idCampus}/{idUniversity}")
//	public Campus linkUniversity(@PathParam("idCampus") int idCampus,
//			@PathParam("idUniversity") int idUniversity) {
//
//		University university = universityService.getUniversity(idUniversity);
//		Campus campus = campusService.getCampus(idCampus);
//		university.setCampus(campus);
//		return university.getCampus();
//	}
	@GetMapping("/map")
	public Map testMap() {

		// Map vehicles = new HashMap<>();
		// vehicles.put("BMW", 5);
		// vehicles.put("Mercedes", 3);
		// vehicles.put("Audi", 4);
		// vehicles.put("Ford", 6);

		Map universitiesMap = new HashMap<>();
		List<University> list = universityRepo.findAll();

		for (University university : list) {
			universitiesMap.put(university.getId(), university);
		}
		// System.out.println("---------------------------Total vehicles: " +
		// vehicles.size());
		//
		// for(Object key: vehicles.keySet()) {
		// System.out.println(key + " -- " + vehicles.get(key));
		// }
		//

		return universitiesMap;
	}
}
