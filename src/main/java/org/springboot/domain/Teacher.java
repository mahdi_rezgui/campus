package org.springboot.domain;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "TEACHER")
@PrimaryKeyJoinColumn(name = "person_id")
public class Teacher extends Person{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Double salary;

	@OneToMany
	@JoinColumn(name = "teacher_id")
	private List<Diplome> listDiplome;
	public Teacher(int id, String first_name, String lastName, String email, String phone, LocalDate birthDate,
			String adress, String login, String password, Double salary) {
		super(id, first_name, lastName, email, phone, birthDate, adress, login, password);
		this.salary = salary;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	} 

}
