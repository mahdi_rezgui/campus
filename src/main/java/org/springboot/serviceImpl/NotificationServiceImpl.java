package org.springboot.serviceImpl;

import org.springboot.domain.Notification;
import org.springboot.repository.NotificationRepository;
import org.springboot.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class NotificationServiceImpl implements NotificationService{

	@Autowired
	private NotificationRepository notificationRepository;

	
	public Notification saveNotification(Notification notification) {
		return notificationRepository.save(notification);
	}

	public Page<Notification> getLastNotificationsByAdmin(int idAdmin, int page, int size) {
		return notificationRepository.getLastNotificationsByAdmin(idAdmin, new PageRequest(page, size));
	}

}
