package org.springboot.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)

public class Candidate extends UserInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private LocalDateTime subsciptionDateTime;
	private String diploma;
	private String curriculumVitae;

	public LocalDateTime getSubsciptionDateTime() {
		return subsciptionDateTime;
	}

	public void setSubsciptionDateTime(LocalDateTime subsciptionDateTime) {
		this.subsciptionDateTime = subsciptionDateTime;
	}

	public String getDiploma() {
		return diploma;
	}

	public void setDiploma(String diploma) {
		this.diploma = diploma;
	}

	public String getCurriculumVitae() {
		return curriculumVitae;
	}

	public void setCurriculumVitae(String curriculumVitae) {
		this.curriculumVitae = curriculumVitae;
	}

}
